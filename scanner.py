from packet_serialize import *
import socket
from socket import *
from time import time, sleep
from random import randint


class Scanner:
    def __init__(self, target, delay=2, prev=False, progress=lambda: None):
        self.delay = delay
        self.my_res = int.from_bytes(inet_aton(gethostbyname(gethostname())), 'big')
        self.target_res = int.from_bytes(inet_aton(gethostbyname(target)), 'big')
        self.target = gethostbyname(target)
        self.progress = progress

        if not prev:
            return
        self.send_sock = socket(AF_INET, SOCK_RAW, IPPROTO_TCP)
        self.send_sock.connect((gethostbyname(target), 0))

        self.listen_sock = socket(AF_PACKET, SOCK_RAW, ntohs(3))
        self.listen_sock.settimeout(delay / 2)

    def connect_scan(self, ports):
        open_ports = []

        for port in ports:
            with socket(AF_INET, SOCK_STREAM) as sock:
                sock.settimeout(self.delay)
                try:
                    sock.connect((self.target, port))
                    open_ports.append(port)
                except OSError as err:
                    continue

        return {'open': open_ports}

    def prev_scan(self, ports, generate_request, tag_response):
        answered_ports, pending_ports = {}, {}

        for port in ports:
            pending_ports[port] = randint(49152, 65535)
            last_transmit_time = time()
            self.send_sock.send(generate_request(pending_ports[port], port))
            while time() - last_transmit_time < self.delay:
                try:
                    buffer = self.listen_sock.recv(65535)
                except timeout:
                    continue
                response = extract_frame(buffer)
                if type(response) is ethernet_frame and \
                        type(response.payload) is ip_datagram and \
                        type(response.payload.payload) is tcp_segment:
                    response = response.payload.payload
                    if response.source in pending_ports and response.dest == pending_ports[response.source]:
                        # print(response)
                        if response.source not in answered_ports:
                            answered_ports[response.source] = tag_response(response)
                        del pending_ports[response.source]
                        break

            sleep(max(0.0, self.delay - (time() - last_transmit_time)))
            self.progress()

        return answered_ports, pending_ports.keys()

    def syn_scan(self, ports):
        def generate_request(source, dest):
            return tcp_segment(self.my_res, self.target_res, source, dest, randint(0, 2 ** 32 - 1), 0,
                               TcpFlags.SYN).serialize()

        def tag_response(seg: tcp_segment):
            return 'open' if TcpFlags.SYN | TcpFlags.ACK in seg.flags else None

        answered, unanswered = self.prev_scan(ports, generate_request, tag_response)
        open_ports = [p for p, tag in answered.items() if tag == 'open']
        return {'open': open_ports}

    def ack_scan(self, ports):
        def generate_request(source, dest):
            return tcp_segment(self.my_res, self.target_res, source, dest, randint(0, 2 ** 32 - 1), 0,
                               TcpFlags.ACK).serialize()

        def tag_response(seg: tcp_segment):
            return 'unf' if TcpFlags.RST in seg.flags else 'fil'

        answered, unanswered = self.prev_scan(ports, generate_request, tag_response)
        unfiltered = [p for p, tag in answered.items() if tag == 'unf']
        filtered = [p for p, tag in answered.items() if tag == 'fil'] + list(unanswered)
        return {'unfiltered': unfiltered, 'filtered': filtered}

    def window_scan(self, ports):
        def generate_request(source, dest):
            return tcp_segment(self.my_res, self.target_res, source, dest, randint(0, 2 ** 32 - 1), 0,
                               TcpFlags.ACK).serialize()

        def tag_response(seg: tcp_segment):
            if TcpFlags.RST in seg.flags:
                if seg.window_size != 0:
                    return 'open'
                else:
                    return 'closed'
            return None

        answered, unanswered = self.prev_scan(ports, generate_request, tag_response)
        open_ports = [p for p, tag in answered.items() if tag == 'open']
        closed_ports = [p for p, tag in answered.items() if tag == 'closed']
        return {'open': open_ports, 'closed': closed_ports, 'filtered': list(unanswered)}

    def fin_scan(self, ports):
        def generate_request(source, dest):
            return tcp_segment(self.my_res, self.target_res, source, dest, randint(0, 2 ** 32 - 1), 0,
                               TcpFlags.FIN).serialize()

        def tag_response(seg: tcp_segment):
            return 'closed' if TcpFlags.RST in seg.flags else None

        answered, unanswered = self.prev_scan(ports, generate_request, tag_response)
        closed_ports = [p for p, tag in answered.items() if tag == 'closed']
        return {'closed': closed_ports, 'filtered|open': list(unanswered)}

    def close(self):
        self.send_sock.close()
        self.listen_sock.close()
