from scanner import Scanner
from ports import services
import argparse
import textwrap
import socket


def parse_range(string: str):
    try:
        parts = list(map(int, string.split('-')))
        if len(parts) != 2:
            raise ValueError()
        return range(parts[0], parts[1])
    except:
        raise argparse.ArgumentTypeError(f'Invalid range {string}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Scan range of ports using a specified technique')
    parser.add_argument('-t', '--target', help='target address', required=True)
    parser.add_argument('-p', '--ports', help='part range to scan', type=parse_range, required=True)
    parser.add_argument('-s', '--scan', help='scan type', choices=['cs', 'ss', 'as', 'fs', 'ws'], required=True)
    parser.add_argument('-d', '--delay', help='delay', type=float, default=2)
    args = parser.parse_args()

    def show_progress():
        print('.', end='', flush=True)
        show_progress.i += 1
        if show_progress.i % 50 == 0:
            print()
    show_progress.i = 0

    print('Scan started')

    try:
        scanner = Scanner(args.target, args.delay, prev=args.scan != 'cs', progress=show_progress)
        results = {}
        if args.scan == 'cs':
            results = scanner.connect_scan(args.ports)
        elif args.scan == 'ss':
            results = scanner.syn_scan(args.ports)
        elif args.scan == 'as':
            results = scanner.ack_scan(args.ports)
        elif args.scan == 'ws':
            results = scanner.window_scan(args.ports)
        elif args.scan == 'fs':
            results = scanner.fin_scan(args.ports)

        print('\nScan results:')
        for tag in results:
            print(f'    {len(results[tag])} {tag} ports:')
            string = ', '.join([str(p) + ('(' + services[p] + ')' if p in services else '') for p in results[tag]])
            print(textwrap.indent(textwrap.fill(string, 70), 8 * ' '))

    except socket.gaierror as err:
        print(err)
    except KeyboardInterrupt:
        print('Exiting')


